var reloadAlarms = function() {
  // run ajax request to refresh action
  $.get('alarms/refresh', function(data) {
    // data is the contents of the rendered partial from controller
    console.log(data);
    // use .html() to replace the html inside the tbody with id alarmstbody
    $("#alarmstbody").html(data);
    setTimeout(reloadAlarms, 10000);
  });
};
var ready = function() {
  // setting timer to realod with function above
  setTimeout(reloadAlarms, 10000);
  // reloading on click of refresh link
  $("#refresh").on('click', reloadAlarms);
};


// document ready call the ready function
$(document).ready(ready);
// this is a dumb thing like document ready when you have turbolinks
$(document).on('page:load', ready);
