class NotificationsController < ApplicationController
before_filter :get_alarm, only: [:index, :show, :edit, :update, :destroy]
# :get_alarm is defined at the bottom of the file,
# and takes the alarm_id given by the routing and
# converts it to an @alarm object.
def refresh
    # render the partial for notifications table back to the ajax request
    # which will be the contents of data and be inserted into the tbody
#    render :partial => 'notifications/notifications_table', :format => 'text/html', :locals => {:notifications => Notification.all}
 #render :partial => 'notifications/notifications_table', :format => 'text/html', :locals => {:notifications => @alarm.notifications}
    #render :partial => 'notifications/notifications_table', :format => 'text/html', locals: { notifications: @alarm.notifications}

  end
def all
@all_notifications=Notification.all
end

def index
@notifications = @alarm.notifications
# was @notifications = Notification.find(:all)
	respond_to do |format|
	format.html # index.html.erb
	format.json { render json: @notifications }
	end
end
# GET /notifications/1
# GET /notifications/1.json
def show
@notification = @alarm.notifications.find(params[:id])
# was Notification.find(params[:id])
	respond_to do |format|
	format.html # show.html.erb
	format.json { render json: @notification }
	end
end
# GET /notifications/new
# GET /notifications/new.json
def new
@alarm = Alarm.find(params[:alarm_id])
@notification = @alarm.notifications.build
# was @notification = Notification.new
	respond_to do |format|
	format.html # new.html.erb
	format.json { render json: @notification }
	end
end
# GET /notifications/1/edit
def edit
@notification = @alarm.notifications.find(params[:id])
# was @notification = Notification.find(params[:id])
end
# POST /notifications
# POST /notifications.json
def create
	# @notification = @alarm.notifications.build(params[:notification])
	 @notification = @alarm.notifications.build(notification_params)

	# was @notification = Notification.new(params[:notification])
	respond_to do |format|
		if @notification.save
		format.html { redirect_to alarm_notifications_url(@alarm), notice:
		'Notification was successfully created.' }
		# was redirect_to(@notification)
		format.json { render json: @notification, status: :created, location: @notification }
		else
		format.html { render action: "new" }
		format.json { render json: @notification.errors, status: :unprocessable_entity }
		end
	end	
end
# PUT /notifications/1
# PUT /notifications/1.json
def update
	@notification = @alarm.notifications.find(params[:id])
	# was @notification = Notification.find(params[:id])
	respond_to do |format|
		if @notification.update_attributes(notification_params)
		format.html { redirect_to alarm_notifications_url(@alarm), notice:
		'Notification was successfully updated.' }
		# was redirect_to(@notification)
		format.json { head :ok }
		else
		format.html { render action: "edit" }
		format.json { render json: @notification.errors, status: :unprocessable_entity }
		end
	end
end
# DELETE /notifications/1
# DELETE /notifications/1.json
def destroy
@notification = @alarm.notifications.find(params[:id])
# was @notification = Notification.find(params[:id])
@notification.destroy
respond_to do |format|
format.html { redirect_to (alarm_notifications_path(@alarm)) }
# was redirect_to(notifications_url)
format.json { head :ok }
	end
end
private
# get_alarm converts the alarm_id given by the routing
# into an @alarm object, for use here and in the view.
	def get_alarm
	@alarm = Alarm.find(params[:alarm_id])
	end
	def notification_params
    params.require(:notification).permit(:id, :ip, :zones, :alarm_id, :alert)
  end
end
