class Alarm < ActiveRecord::Base	
	
	has_many :notifications, :dependent => :destroy
	def name
	first_name + " " + last_name
	end
	validates :first_name,presence: true, length:{:in=>2..50, message: "Debe tener entre 2 y 50 caracateres"}
	validates :last_name,presence: true, length:{:in=>2..50, :message => "Debe tener entre 2 y 50 caracateres"}
	validates :email,presence: true, length:{:in=>2..50, :message => "Debe tener entre 2 y 50 caracateres"}
	validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

	validates :key,presence: true,:numericality => { :only_integer => true , :message => "Debe contener solo numeros"}
	validates :phone,presence: true,:numericality => { :only_integer => true , :message => "Debe contener solo numeros"}
end
