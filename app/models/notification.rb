class Notification < ActiveRecord::Base
	belongs_to :alarm
	validates_existence_of :alarm, :both => false
end
