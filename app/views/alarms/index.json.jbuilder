json.array!(@students) do |student|
  json.extract! student, :id, :first_name, :email, :last_name, :address, :key, :phone
  json.url student_url(student, format: :json)
end
