json.array!(@awards) do |award|
  json.extract! award, :id, :first_name, :zones, :student_id
  json.url award_url(award, format: :json)
end
