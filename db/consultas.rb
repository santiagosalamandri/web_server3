require 'socket'                # Get sockets from stdlib
require 'rubygems'
require 'sqlite3'

class String
def to_bool
    return true if self == true || self =~ (/^(true|t|yes|y|1)$/i)
    return false if self == false || self.blank? || self =~ (/^(false|f|no|n|0)$/i)
    raise ArgumentError.new("invalid value for Boolean: \"#{self}\"")
  end
end
	DBNAME = "development.sqlite3"
server = TCPServer.open(60000)   # Socket to listen on port 6000
	loop {                         
	# Servers run forever
	  Thread.start(server.accept) do |client|

	   while true
	   	DB ||= SQLite3::Database.open( DBNAME )
		    DB.results_as_hash = true
		puts("conexion desde IP:",client.peeraddr[3],"\n")
		#entrada=client.gets.chomp
		entrada=client.recv(50)
	if((entrada.count ",")!=4)
			puts('formato incorrecto..cerrando conexion de ',client.peeraddr[3])
			client.puts('formato incorrecto')
			client.close	
	else	
		
		values=entrada.split(",",5)
		id=values[0].to_i
		user=values[1].to_s
		alert=values[2].to_i
		zones=values[3]					
		ip_dir=client.peeraddr[3].to_s.chomp
		created=Time.now.to_s					
		updated=Time.now.to_s
		print values

		puts 'empezando query'
		consulta=DB.prepare("SELECT id,first_name,key FROM alarms")
		rs=consulta.execute
		puts 'finalizando query'
		i=0
		key=nil
		puts 'inicio busqueda de id'
		rs.each do |row|
		#printf "fila %d,%s\n",i,row['first_name']
			if(row['id']==id)
				key=row['first_name']
				puts 'clave encontrada!',key
			end
		i=i+1
    end
        puts "termino busqueda"
	
		if key==nil
			puts "no se encontro usuario"
			client.puts('usuario incorrecto')
			client.close	
		else	

			if(key==user)
				puts "autenticacion OK"
				puts "insertando notificacion"
				#inserto elemento si autentico OK
				DB.execute("INSERT INTO notifications (ip, zones, alarm_id, created_at, updated_at, alert) 
					VALUES (?, ?, ?, ?, ?, ?)",
					[ip_dir,zones,id,created,updated,alert])
				puts "Notificacion insertada!"
				client.puts('OK')
				client.close
			else
				puts "verificacion FALLO"
				client.puts('fallo autenticacion')
				client.close
			end				
		end	
	end
			
	end
end

	}