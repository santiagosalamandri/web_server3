class ChangeIntSize < ActiveRecord::Migration
  	
  	def change
  	change_column(:alarms, :key, :integer, limit: 8)
	change_column(:alarms, :phone, :integer, limit: 8)
    end
end