class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :given_name
      t.string :middle_name
      t.string :family_name
      t.date :date_of_birth
      t.decimal :key
      t.date :phone

      t.timestamps null: false
    end
  end
end
