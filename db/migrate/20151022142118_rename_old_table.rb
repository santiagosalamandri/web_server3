class RenameOldTable < ActiveRecord::Migration
  def change
    rename_table :awards, :notifications
    rename_table :students, :alarms
  end
  
end
