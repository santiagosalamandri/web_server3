class ChangeColumns < ActiveRecord::Migration
  def change
        rename_column :notifications, :name, :ip
        rename_column :notifications, :zones, :zones
              
        rename_column :alarms, :given_name, :name
        rename_column :alarms, :middle_name, :email
        rename_column :alarms, :family_name, :last_name
        rename_column :alarms, :date_of_birth, :address
        rename_column :alarms, :key, :key
        rename_column :alarms, :phone, :phone

  end
end
