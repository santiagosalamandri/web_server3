class ChangeColumnsTypes < ActiveRecord::Migration
  def change
  change_column :notifications, :zones, :string
  change_column :alarms, :address, :string
  change_column :alarms, :key, :string
  change_column :alarms, :phone, :integer
  
  end
end
