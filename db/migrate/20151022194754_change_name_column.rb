class ChangeNameColumn < ActiveRecord::Migration
  def change
            rename_column :alarms, :name, :first_name
  end
end
