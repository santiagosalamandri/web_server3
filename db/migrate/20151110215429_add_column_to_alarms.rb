class AddColumnToAlarms < ActiveRecord::Migration
  def change
    add_column :alarms, :alert, :bool
  end
end
