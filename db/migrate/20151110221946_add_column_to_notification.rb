class AddColumnToNotification < ActiveRecord::Migration
  def change
    add_column :notifications, :alert, :bool
  end
end
